package com.hoc.bai10;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import java.text.DecimalFormat;
import java.util.Locale;

import static java.lang.Double.parseDouble;
import static java.util.Objects.requireNonNull;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");
    private TextInputLayout mTextInputLayout1, mTextInputLayout2;
    private TextView mTextViewResult;
    private Button mButtonAdd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();

        addTextChangedListener(mTextInputLayout1);
        addTextChangedListener(mTextInputLayout2);

        mButtonAdd.setOnClickListener(this);
    }

    private void findViews() {
        mTextInputLayout1 = findViewById(R.id.text_input_layout_1);
        mTextInputLayout2 = findViewById(R.id.text_input_layout_2);
        mTextViewResult = findViewById(R.id.text_result);
        mButtonAdd = findViewById(R.id.button_add);
    }

    private void addTextChangedListener(@NonNull TextInputLayout inputLayout) {
        requireNonNull(inputLayout.getEditText()).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    Double.parseDouble(s.toString());
                    mTextInputLayout1.setError(null);
                } catch (NumberFormatException e) {
                    mTextInputLayout1.setError("Wrong number format!");
                }
            }
        });
    }

    public void onButtonAddClick() {
        double numA;
        try {
            String string = requireNonNull(mTextInputLayout1.getEditText()).getText().toString();
            numA = parseDouble(string);
        } catch (NumberFormatException e) {
            return;
        }

        double numB;
        try {
            String string = requireNonNull(mTextInputLayout2.getEditText()).getText().toString();
            numB = parseDouble(string);
        } catch (NumberFormatException e) {
            return;
        }

        mTextViewResult.setText(DECIMAL_FORMAT.format(numA + numB));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_add) {
            onButtonAddClick();
        }
    }
}
